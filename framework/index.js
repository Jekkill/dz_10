import { MailValidator } from './services/index';

const apiProvider = () => ({
  email: () => new MailValidator(),
});

export { apiProvider };
