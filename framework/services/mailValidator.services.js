import supertest from 'supertest';
import { urls, tokens } from '../config/index';

const MailValidator = function mailValidator() {
  this.validateEmail = async function validateEmail(email) {
    const r = await supertest(urls.mailValidator)
      .get(`/check?access_key=${tokens.mailValidator}&email=${email}`)
      .set('Accept', 'application/json');
    return r;
  };
  this.validateEmailWithoutApiKey = async function validateEmailWithoutApiKey(email) {
    const r = await supertest(urls.mailValidator)
      .get(`/check?email=${email}&smtp=1&format=1`)
      .set('Accept', 'application/json');
    return r;
  };
};
export { MailValidator };
