import { expect, test } from '@jest/globals';
import { email } from '../framework/config/index';
import { apiProvider } from '../framework';

describe('Проверка http запросов для mailvalidator', () => {
  test('Отправляем валидный email', async () => {
    const r = await apiProvider().email().validateEmail(email.validEmail);
    expect(r.status).toEqual(200);
    expect(r.body.format_valid).toEqual(true);
  });
  test('Отправляем email без API ключа', async () => {
    const { status, body } = await apiProvider().email()
      .validateEmailWithoutApiKey(email.validEmail);
    expect(status).toEqual(200);
    expect(body.error.type).toEqual('missing_access_key');
  });
});

describe('Проверка параметризированных запросов', () => {
  test.each`
    requestedEmail            | name                                       | bodyField             | expectedResult
    ${'test@mail.com'}        | ${'Формат верный'}                         | ${'format_valid'}     | ${true}
    ${'aaabbbccc'}            | ${'Формат неверный'}                       | ${'format_valid'}     | ${false}
    ${'support@apilayer.com'} | ${'Выводим пользовательскую часть (до @)'} | ${'user'}             | ${'support'}
    ${'support@apilayer.com'} | ${'Выводим домен (после @)'}               | ${'domain'}           | ${'apilayer.com'}
   `('$requestedEmail - $name', async({
       requestedEmail, name, bodyField, expectedResult
    }) => {
    const r = await apiProvider().email().validateEmail(requestedEmail);
    expect(r.body[bodyField]).toEqual(expectedResult);
  });
});
